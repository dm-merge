
CC=gcc
RM=rm -f
CFLAGS_COMMON=-Wall -pedantic
CFLAGS_OPTI=-O3 -fomit-frame-pointer -ffast-math
CFLAGS_DEBUG=-ggdb3
ifeq "$(ARG)" "opti"
    export CFLAGS=$(CFLAGS_COMMON) $(CFLAGS_OPTI)
else
    export CFLAGS=$(CFLAGS_COMMON) $(CFLAGS_DEBUG)
endif

NAME=dm-merge

LIBS=
OBJS=dm-merge.o 

$(NAME): $(OBJS) $(LIBS)
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c %.h
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	$(RM) *.o $(NAME) core

